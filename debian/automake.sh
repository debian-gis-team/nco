#!/bin/sh
# automake requires --foreign to not fail the autoreconf process
automake --foreign "$@"
